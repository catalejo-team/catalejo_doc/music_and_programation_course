.. _ultrasonic_example:

Uso de sensor ultrasonido con ChucK y Arduino
=============================================

Montaje
+++++++

.. todo::

   Monte el siguiente circuito teniendo en cuenta el :ref:`arduino_uno_pinout` correctamente.

.. image:: img/ultrasonic_device_bb.png


Código Arduino
++++++++++++++

.. todo::

   Guarde el siguiente programa en el Arduino y cierre el Arduino IDE para liberar el hardware
   dejándolo habilitado para ChucK.

.. code-block:: c

   #include <NewPing.h>

   //Pin donde se pondrá el disparador del ultrasonido
   #define TRIGGER_PIN 3
   //Pin donde se pondrá el eco
   #define ECHO_PIN 2
   //Definir máxima distancia en centímetros
   #define MAX_DISTANCE 100

   //Creación de configuración para ultrasonido
   NewPing sensor(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);

   void setup(){
       //Inicia la comunicación con el computador
       //Velocidad:
       //  lenta:  9600 baudios
       //  alta:   115200 baudios
       Serial.begin(9600);
   }

   // Inicio de programa pincipal
   void loop(){
       // Cada 100ms se hace una toma de la distancia
       delay(100);
       //Se envía el valor leído del sensor al computador
       Serial.println(sensor.ping_cm());
   }

Código ChucK
++++++++++++

.. todo::

   Ejecute el siguiente programa en ChucK, observe el resultado en la consola de ChucK e interactúe
   con el sensor ultrasonido acercando y alejando su mano de él.

.. code-block:: shell

   /*
   * Listando dispositivos seriales
   * escoger el indice que representa el
   * dispositivo serial.
   */
   SerialIO.list() @=> string list[];
   for(int i; i < list.size(); i++){
      <<< i, ":", list[i] >>>;
   }

   SinOsc miOsc => dac;
   0.3 => miOsc.gain;

   SerialIO arduinoCom;
   arduinoCom.open(0, SerialIO.B9600, SerialIO.ASCII);

   while(true){
       arduinoCom.onLine() => now;
       arduinoCom.getLine() => string line;
       <<<line>>>;
       Std.atoi(line)*20+200 => miOsc.freq;
   }
