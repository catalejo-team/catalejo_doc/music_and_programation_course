.. _chuck_wav_sensors:

Sonidos en formato wav, sensores análógicos y digitales
=======================================================

Código de ejemplo
+++++++++++++++++

Código ChucK
------------

.. literalinclude:: wav_and_sensors/digitalSensors.ck
   :linenos:


Código Arduino
--------------

.. literalinclude:: wav_and_sensors/analog2digital_and_reads_push_button.ino
   :language: c
   :emphasize-lines: 2
   :linenos:


