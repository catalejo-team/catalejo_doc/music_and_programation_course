.. Curso de programación y música documentation master file, created by
   sphinx-quickstart on Thu Sep 20 11:28:59 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenido al Curso de programación y música!
=============================================

.. image:: catalejo.jpg
   :scale: 50 %

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   chuck/chuck_index
   arduino/arduino
   chuck_arduino/chuck_arduino_index
   complements/complements_index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
