.. _arduino_hello_word:

Arduino, !Hola mundo!
=====================

.. _aquí en video: https://youtu.be/G0hsmPYrkSM

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/G0hsmPYrkSM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

.. attention::
   **¿No puedes ver el vídeo?** Mantén presionado `aquí en video`_ hasta que se habilite la opción de abrirlo en la app de YOUTUBE.

.. note::
   * digitalWrite(pin, estado), estado[1, 0] (HIGH, LOW).

   * sleep(microsegundos)

.. _example_code_hello_word:

Código del blink
++++++++++++++++

.. code-block:: c

   void setup() {
     // put your setup code here, to run once:
     pinMode(13, OUTPUT);
     Serial.begin(9600);
   }

   void loop() {
     // put your main code here, to run repeatedly:
     digitalWrite(13, HIGH);
     delay(1000);
     Serial.println("Led On");
     digitalWrite(13, LOW);
     delay(1000);
     Serial.println("Led Off");
   }

.. todo::
   * Cambia el tiempo de encendido y de apagado del LED.

   * Cambia el mensaje que sale por el monitor serial.
