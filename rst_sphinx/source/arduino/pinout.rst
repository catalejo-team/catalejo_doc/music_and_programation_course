.. _arduino_uno_pinout:

Pinout Arduino UNO
==================

Revisión V1
+++++++++++

.. image:: img/pinout_arduino_uno.png

Revisión V3
+++++++++++

.. image:: img/pinout_arduino_uno_v3.png
