.. _links_of_interest:

Links de interés
================


Donal en la tierra mágica de la matemática
++++++++++++++++++++++++++++++++++++++++++

Las matemáticas en el mundo del arte, arquitectura, escultura, pintura, el número aureo en la naturaleza y más.

.. _aquí en video: https://youtu.be/rJkdjL21Tqs

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/rJkdjL21Tqs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

.. attention::
   **¿No puedes ver el vídeo?** Mantén presionado `aquí en video`_ hasta que se habilite la opción de abrirlo en la app de YOUTUBE.

.. todo::
   ¿De qué manera se puede encontrar el número Aureo?

.. _chuck_interest:

ChucK
+++++

.. _chuck_oficial_link: http://chuck.cs.princeton.edu/
.. _chuck_oficial_download: http://chuck.cs.princeton.edu/release/

Chuck es un lenguaje de programación que permite sintetizar sonidos y crear música.
Links:

   * `chuck_oficial_link`_.

   * `chuck_oficial_download`_.

.. _musescore_interest:

Musescore
+++++++++

.. _musescore: https://musescore.org/es
.. _musescore_download: https://musescore.org/es/download

Con `musescore`_ puedes crear, reproducir e imprimir tus partiruras.
Éste programa es multiplataforma (Windows, MacOs, GNU Linux), se descarga de manera gratuita.

Puedes descargar musescore desde aquí: `musescore_download`_.

LMMS
++++

.. _LMMS: https://lmms.io/
.. _LMMS_download: https://lmms.io/download/

Con `LMMS`_ puedes producir música digital desde la PC, conectando y desconectado diferentes elementos
que sintetizan diferentes sonidos, pudesde descargarlo desde aquí: `LMMS_download`_.

Freesound
+++++++++

.. _Freesound: https://freesound.org/

`Freesound`_ es un repositorio de muestras de audio con licencia CC, sin ánimo de lucro, donde puedes guardar, reproducir y descargar tales muestras de manera libre y
sin costo.
