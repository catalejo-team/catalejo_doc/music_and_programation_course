Mi primer tono
==============

.. _aquí en video: https://youtu.be/NbijeqwrbJg

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/NbijeqwrbJg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

.. attention::
   **¿No puedes ver el vídeo?** Mantén presionado `aquí en video`_ hasta que se habilite la opción de abrirlo en la app de YOUTUBE.

.. note::
   * SinOsc: Oscilador de señal seno.
   * SqrOsc: Oscilador de señal cuadrada.
   * TriOsc: Oscilador de señal triangular.
   * SawOsc: Oscilador de señal diente de sierra.

.. note::
   * .gain, va desde 0.0 hasta 1.0
   * .freq, va desde 0 hasta 20000 Hz (20 kilo hertz).
   * x :: sencond => now, donde x es el tiempo que durará el sonido.

Código de ejemplo
+++++++++++++++++

.. code-block:: c

   SinOsc miOscilador => dac;
   0.5 => miOscilador.gain;
   300 => miOscilador.freq;
   2.0 :: second => now;


.. todo::
   * Pruebe cambiando tanto el volumen, la frecuencia y la duración.
   * Pruebe cambiado el tipo de oscilador.
   * Genere un silencio.

.. todo::
   ¿Qué pasaría si pones el código como sigue?

.. code-block:: c

   SinOsc miOscilador => dac;
   0.5 => miOscilador.gain, 300 => miOscilador.freq, 2.0 :: second => now;
