.. _octave_and_midi_numbers:

Octavas y sus números MIDI
==========================

Notas musicales de la escala de Sol
+++++++++++++++++++++++++++++++++++

.. image:: img/musicalNotes.jpg

Las notas musicales y sus números MIDI
++++++++++++++++++++++++++++++++++++++

+-----------+---------------------------------------------------------+
|   NOTAS   |                                                         |
|           | OCTAVA                                                  |
|           +----+----+----+----+----+----+----+----+-----+-----+-----+
|           | -1 | 0  | 1  | 2  | 3  | 4  | 5  | 6  | 7   | 8   | 9   |
+======+====+====+====+====+====+====+====+====+====+=====+=====+=====+
| Do   | C  | 0  | 12 | 24 | 36 | 48 | 60 | 72 | 84 | 96  | 108 | 120 |
+------+----+----+----+----+----+----+----+----+----+-----+-----+-----+
| Do#  | C# | 1  | 13 | 25 | 37 | 49 | 61 | 73 | 85 | 97  | 109 | 121 |
+------+----+----+----+----+----+----+----+----+----+-----+-----+-----+
| Re   | D  | 2  | 14 | 26 | 38 | 50 | 62 | 74 | 86 | 98  | 110 | 122 |
+------+----+----+----+----+----+----+----+----+----+-----+-----+-----+
| Re#  | D# | 3  | 15 | 27 | 39 | 51 | 63 | 75 | 87 | 99  | 111 | 123 |
+------+----+----+----+----+----+----+----+----+----+-----+-----+-----+
| Mi   | E  | 4  | 16 | 28 | 40 | 52 | 64 | 76 | 88 | 100 | 112 | 124 |
+------+----+----+----+----+----+----+----+----+----+-----+-----+-----+
| Fa   | F  | 5  | 17 | 29 | 41 | 53 | 65 | 77 | 89 | 101 | 113 | 125 |
+------+----+----+----+----+----+----+----+----+----+-----+-----+-----+
| Fa#  | F# | 6  | 18 | 30 | 42 | 54 | 66 | 78 | 90 | 102 | 114 | 126 |
+------+----+----+----+----+----+----+----+----+----+-----+-----+-----+
| Sol  | G  | 7  | 19 | 31 | 43 | 55 | 67 | 79 | 91 | 103 | 115 | 127 |
+------+----+----+----+----+----+----+----+----+----+-----+-----+-----+
| Sol# | G# | 8  | 20 | 32 | 44 | 56 | 68 | 80 | 92 | 104 | 116 |     |
+------+----+----+----+----+----+----+----+----+----+-----+-----+-----+
| La   | A  | 9  | 21 | 33 | 45 | 57 | 69 | 81 | 93 | 105 | 117 |     |
+------+----+----+----+----+----+----+----+----+----+-----+-----+-----+
| La#  | A# | 10 | 22 | 34 | 46 | 58 | 70 | 82 | 94 | 106 | 118 |     |
+------+----+----+----+----+----+----+----+----+----+-----+-----+-----+
| Si   | B  | 11 | 23 | 35 | 47 | 59 | 71 | 83 | 95 | 107 | 119 |     |
+------+----+----+----+----+----+----+----+----+----+-----+-----+-----+

.. note::
   Para poder "tocar" una nota de la escala musical, se requiere el siguiente comando:

   **x => Std.mtof**, donde **x** es cualquiera de los número mostrados en la tabla anterior.

Para saber la frecuencia de una nota representada con el número midi, puede hacer lo siguiente:

.. code-block:: c

   <<<"Frecuencia de la nota Do 4° octava">>>;
   <<<Std.mtof(60)>>>;

Tocando una nota en la representación MIDI:
+++++++++++++++++++++++++++++++++++++++++++

A continuación se muestra como tocar una nota Do de la 4° octava; puede hacer algo similar
para las demás notas.

.. code-block:: c

   SinOsc miOsc => dac;
   <<<"Do 4° octava">>>;
   60 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, 1 :: second => now;

Código de ejemplo de uso:
+++++++++++++++++++++++++

Veamos como se toca una escala musical en la 4° octava (Do, Re, Mi, Fa, Sol, La, Si):

.. code-block:: c

   // Crear un oscilador seno
   // y conectarlo a la tarjeta de audio
   SinOsc miOsc => dac;
   // Volumen al 50 %
   0.5 => miOsc.gain;
   <<<"Do 4° octava">>>;
   60 => Std.mtof => miOsc.freq, 1 :: second => now;
   <<<"Re 4° octava">>>;
   62 => Std.mtof => miOsc.freq, 1 :: second => now;
   <<<"Mi 4° octava">>>;
   64 => Std.mtof => miOsc.freq, 1 :: second => now;
   <<<"Fa 4° octava">>>;
   65 => Std.mtof => miOsc.freq, 1 :: second => now;
   <<<"Sol 4° octava">>>;
   67 => Std.mtof => miOsc.freq, 1 :: second => now;
   <<<"La 4° octava">>>;
   69 => Std.mtof => miOsc.freq, 1 :: second => now;
   <<<"Si 4° octava">>>;
   71 => Std.mtof => miOsc.freq, 1 :: second => now;

Otra manera de obtener el mismo resultado:

.. code-block:: c

   // Crear un oscilador seno
   // y conectarlo a la tarjeta de audio
   SinOsc miOsc => dac;
   // Volumen al 50 %
   0.5 => miOsc.gain;
   <<<"Do 4° octava">>>;
   Std.mtof(60) => miOsc.freq, 1 :: second => now;
   <<<"Re 4° octava">>>;
   Std.mtof(62) => miOsc.freq, 1 :: second => now;
   <<<"Mi 4° octava">>>;
   Std.mtof(64) => miOsc.freq, 1 :: second => now;
   <<<"Fa 4° octava">>>;
   Std.mtof(65) => miOsc.freq, 1 :: second => now;
   <<<"Sol 4° octava">>>;
   Std.mtof(67) => miOsc.freq, 1 :: second => now;
   <<<"La 4° octava">>>;
   Std.mtof(69) => miOsc.freq, 1 :: second => now;
   <<<"Si 4° octava">>>;
   Std.mtof(71) => miOsc.freq, 1 :: second => now;

