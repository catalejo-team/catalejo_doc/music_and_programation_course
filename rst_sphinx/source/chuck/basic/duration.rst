.. _duration:

Duración y figuras musicales
============================

Figuras musicales y sus tiempos
+++++++++++++++++++++++++++++++

.. image:: img/figureAndsTimes.jpg

Figuras musicales y sus silencios
+++++++++++++++++++++++++++++++++

.. image:: img/silences.jpg

.. note::
   Los silencios duran igual que las notas.

El compás
+++++++++

.. image:: img/compas.png

La notación del compás está definida con un :math:`{numerador \over denominador }` donde el **numerador** indica
la cantidad de tiempos que habrán en cada compás y el **denominador** indica la figura a usar.

Ejemplos de compás
------------------

* :math:`{2 \over 4}`: Habrán **2** tiempos por compás y la figura es la **negra** por el **4** (1/4).

* :math:`{4 \over 8}`: Habrán **4** tiempos por compás y la figura es la **corchea** por el **8** (1/8).

Creación de duraciones según figuras musicales
++++++++++++++++++++++++++++++++++++++++++++++

Nos referiremos a la duración a la cantidad de tiempo que se desea que una nota sea tocada.

.. note::
   Para crear una duración se usa la palabra mágica **dur**;

Si usamos de referencia la figura musical **NEGRA**, los tiempos serían:

* **Redonda**: Entera | 4T

* **Blanca**: Media  | 2T

* **Negra**: Cuarta | 1T

* **Corchea**: Octava | 1/2T

* **Semicorchea**: Dieciseisava | 1/4T

* **Fusa**: Treintaidosava | 1/8T

* **Semifusa**: Sesentacuatroava | 1/16T

Y podríamos crear los tiempos correspondientes:

.. code-block:: c

   //Asignación de la duración del compas, por ejemplo, 1 segundo
   1.0 :: second => dur compas;
   compas/2 => dur Negra; //Cuarta
   4*Negra => dur Redonda; //Entera
   2*Negra => dur Blanca; //Media
   Negra/2 => dur Corchea; //Octava
   Negra/4 => dur Semicorchea; //Dieciseisava
   Negra/8 => dur Fusa; //Treintaidosava
   Negra/16 => dur Semifusa; //Sesentacuatroava

De manera simplificada:

.. code-block:: c

   //Asignación de la duración del compas, por ejemplo, 1 segundo
   1.0 :: second => dur compas;
   compas/2 => dur N; //Negra
   4*N => dur R; //Redonda
   2*N => dur B; //Blanca
   N/2 => dur C; //Corchea
   N/4 => dur S; //Semicorchea
   N/8 => dur F; //Fusa
   N/16 => dur SF; //Semifusa


Ejemplo de uso:
+++++++++++++++

Ejemplo 1
---------

.. code-block:: c

   //Asignación de la duración del compas, por ejemplo, 1 segundo
   1.0 :: second => dur compas;
   compas/2 => dur Negra; //Cuarta
   4*Negra => dur Redonda; //Entera
   2*Negra => dur Blanca; //Media
   Negra/2 => dur Corchea; //Octava
   Negra/4 => dur Semicorchea; //Dieciseisava
   Negra/8 => dur Fusa; //Treintaidosava
   Negra/16 => dur Semifusa; //Sesentacuatroava

   // Creación de oscilador seno
   SinOsc miOsc => dac;

   //Nota Do en 4° octava
   60 => Std.mtof => miOsc.freq;

   <<<"Durará una octava en Do">>>;
   0.5 => miOsc.gain, Corchea => now;
   <<<"Durará una octava en silencio">>>;
   0 => miOsc.gain, Corchea => now;
   <<<"Durará media en nota Do">>>;
   0.5 => miOsc.gain, Blanca => now;
   <<<"Durará media en silencio">>>;
   0 => miOsc.gain, Blanca => now;
   <<<"Durará una entera en nota Do">>>;
   0.5 => miOsc.gain, Redonda => now;
   <<<"Durará una entera en silencio">>>;
   0 => miOsc.gain, Redonda => now;

Ejemplo 2
---------

.. code-block:: c

   /*
   * Ésto es solo un comentario, no lo tendrá en cuenta Chuck
   * Arbitrariamente se hará la siguiente representación por simplicidad:
   * R = Redonda : Entera,
   * B = Blanca : Media,
   * N = Negra : Cuarta,
   * C = Corchea : Octava,
   * S = Semicorchea : Dieciseisava,
   * F = Fusa : Treintaidosava,
   * SF = Semifusa : Sesentacuatroava.
   */
   //Asignación de la duración del compas, por ejemplo, 0.8 segundos
   0.8 :: second => dur compas;
   compas/2 => dur N; //Negra
   4*N => dur R; //Redonda
   2*N => dur B; //Blanca
   N/2 => dur C; //Corchea
   N/4 => dur S; //Semicorchea
   N/8 => dur F; //Fusa
   N/16 => dur SF; //Semifusa

   // Creación de oscilador seno
   SinOsc miOsc => dac;

   //Nota Do en 4° octava
   60 => Std.mtof => miOsc.freq;

   <<<"Durará una octava en Do">>>;
   0.5 => miOsc.gain, C => now;
   <<<"Durará una octava en silencio">>>;
   0 => miOsc.gain, C => now;
   <<<"Durará media en nota Do">>>;
   0.5 => miOsc.gain, B => now;
   <<<"Durará media en silencio">>>;
   0 => miOsc.gain, B => now;
   <<<"Durará una entera en nota Do">>>;
   0.5 => miOsc.gain, R => now;
   <<<"Durará una entera en silencio">>>;
   0 => miOsc.gain, R => now;

