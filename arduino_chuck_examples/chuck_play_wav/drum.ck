Gain master => dac;
SndBuf kick => master;
SndBuf cymbal => master;
me.dir()+"kick_1.wav" => kick.read;
me.dir()+"acoustic_ride_cymbal.wav" => cymbal.read;
//rate[0.2:1.8]
1 :: second => now;
while (true){
    1 => kick.rate;
    0 => kick.pos;
    0.5 :: second => now;
    0 => cymbal.pos;
    0.3 :: second => now;
    0 => cymbal.pos;
    0.3 :: second => now;
}
