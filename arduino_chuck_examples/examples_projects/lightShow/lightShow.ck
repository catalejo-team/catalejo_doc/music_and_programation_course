/*
 * @file digitalSensors.ck
 * @brief Permite capturar dese el puerto serial diferentes lecturas
 *  de sensores digitales e interpretar desde ChucK.
 * @author: Johnny Cubides
 * contact: catalejoeducacion@gmail.com, jgcubidesc@gmail.com
 * date: Oct 03, 2018
 * license: GPL
 */
//*********** CONFIGURACION PUERTO SERIAL *****************
<<<"Listando puertos seriales">>>;
SerialIO.list() @=> string list[];
for(int i; i < list.size(); i++){
   <<< i, ":", list[i] >>>;
}
<<<"Iniciando puerto serial">>>;
SerialIO arduinoCom;
//Seleccionar el puerto serial de interés
//con el índice de la lista anterior
arduinoCom.open(0, SerialIO.B9600, SerialIO.ASCII);

Gain master => dac;

//*********** CONFIGURACION INSTRUMENTO NOTAS MIDI *******
1 => int MAX_DIGITAL_SENSOR;
SinOsc osc[MAX_DIGITAL_SENSOR];
//Los siguientes 2 arregos corresponden a la posición decada sensor,
//en éste ejemplo son tres sensores que tienen asociadas 3 notas con
//su respectivo volumen.
[60, 62, 64, 65, 67, 69] @=> int midi[];
[0.5, 0.5, 0.5, 0.5, 0.5, 0.5] @=> float volumen[];

for(0 => int i; i< MAX_DIGITAL_SENSOR; i++)
{
    osc[i] => master;
    midi[i] => Std.mtof => osc[i].freq;
    0 => osc[i].gain;
}
50::ms => dur myTime; // Cada cuanto se actualizarán los sonidos

//********** CONFIGURACION DE SONIDOS WAV *****************
1 => int MAX_PUSH_SENSOR;
SndBuf wav[MAX_PUSH_SENSOR];
[
"kick_1.wav"
] @=> string audios[];
for(0 => int i; i< MAX_PUSH_SENSOR; i++)
{
    me.dir()+audios[i] => wav[i].read;
    wav[i] => master;
}
1000::ms => dur myTimeWav; // Cada cuanto se actualizarán los sonidos

// Variables
string line; // Guarda los valores recogidos por el puerto serial
0 => int valueDigital; // Guarda el valor de los sensores digitales
0 => int valuePush; //Elementos a presionar

fun void readSerial()
{

    <<<"ok">>>;
    while(true)
    {
        arduinoCom.onLine() => now;
        arduinoCom.getLine() => line;
        //<<<line>>>;
        if(line == "P")
        {
            arduinoCom.onLine() => now;
            arduinoCom.getLine() => line;
            while(line == ""){
                arduinoCom.onLine() => now;
                arduinoCom.getLine() => line;
            }
            Std.atoi(line) => valueDigital;
            //<<<line>>>;
        }/*else if(line == "P")
        {
            arduinoCom.onLine() => now;
            arduinoCom.getLine() => line;
            while(line == ""){
                arduinoCom.onLine() => now;
                arduinoCom.getLine() => line;
            }
            Std.atoi(line) => valuePush;
            //<<<line>>>;
        }*/
    }
}

fun void playDigitalSond()
{
    while(true)
    {
        for(0 => int i; i< MAX_DIGITAL_SENSOR; i++)
        {
            volumen[i]*(valueDigital & (1 << i)) => osc[i].gain;
        }
        myTime => now;
    }
}

fun void playWavSond()
{
    while(true)
    {
        for(0 => int i; i< MAX_PUSH_SENSOR; i++)
        {
            if(valuePush & (1 << i))
                0 => wav[i].pos;
        }
        myTimeWav => now;
    }
}

spork ~ readSerial();
spork ~ playDigitalSond();
spork ~ playWavSond();

while(true) 1::second => now;