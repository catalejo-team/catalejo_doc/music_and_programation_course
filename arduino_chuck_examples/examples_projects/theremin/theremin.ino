#include <Tone.h>
#include <Adafruit_NeoPixel.h>
#define PIN 2
#define NUMPIXELS 16

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
//int delayval = 10;
int delayval = 1;

struct Sensor_Ultrasonido
{
  int disparo;
  int eco;
  float distancia;
};

struct Parlantes
{
  int izquierdo;
  unsigned long izquierdo_altura;
  int derecho;
  unsigned long derecho_altura;
};

struct Leds
{
  unsigned int R[16];
  unsigned int G[16];
  unsigned int B[16];
};

Sensor_Ultrasonido izquierdo;
Sensor_Ultrasonido derecho;
Parlantes parlante;

Tone tono_izquierdo;

Tone tono_derecho;

Leds leds;
int count = 0;

void setup() {
  // put your setup code here, to run once:
  izquierdo.disparo = 7;
  izquierdo.eco = 6;

  derecho.disparo = 5;
  derecho.eco = 4;

  parlante.izquierdo = 11;
  parlante.derecho = 8 ;

  pinMode(izquierdo.disparo, OUTPUT);
  pinMode(izquierdo.eco, INPUT);
  pinMode(derecho.disparo, OUTPUT);
  pinMode(derecho.eco, INPUT);

  tono_izquierdo.begin(parlante.izquierdo);
  tono_derecho.begin(parlante.derecho);

  pixels.begin();
  Serial.begin(9600);

}

float distancia_cm(int disparo, int eco) {
   float duracion, distancia;
   digitalWrite(disparo, LOW);  //para generar un pulso limpio ponemos a LOW 4us
   delayMicroseconds(5);
   digitalWrite(disparo, HIGH);  //generamos Trigger (disparo) de 10us
   delayMicroseconds(10);
   digitalWrite(disparo, LOW);
   duracion = pulseIn(eco, HIGH);  //medimos el tiempo entre pulsos, en microsegundos
   distancia = duracion * 10.0 / 292.0/ 2.0;   //convertimos a distancia, en cm
   if(distancia < 40 )
   return distancia;
   else
   return 0;
}

/*long ultrasonic(int trigger, int echo){
  digitalWrite(trigger, LOW);
  delayMicroseconds(5);
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10);
  long time_echo = pulseIn(echo, HIGH);
  return long(0.017*time_echo);
}*/

void print_leds(){
  for(int i=0;i<NUMPIXELS;i++){
    //pixels.setPixelColor(i, pixels.Color(0,255,0));
    pixels.setPixelColor(i, pixels.Color(leds.R[i],leds.G[i],leds.B[i]));
    pixels.show();
    delay(delayval);
  }
}


unsigned long tono_grave(float distance){
  //return 100*distance;
  return 30*distance;
}

unsigned long tono_agudo(float distance){
  //return 100*distance;
  return 130*distance;
}

void leds_altura_izquierdo(unsigned long altura){
  altura = altura/68;
  for(int i=0;i<NUMPIXELS;i++){
    if(i < altura){
      leds.R[i]=255;
      leds.B[i]= 0;
    }else{
      leds.R[i]= 0;
      leds.B[i]= 50;
    }
  }
}

void leds_altura_derecho(unsigned long altura){
  altura = altura/250;
  for(int i=0;i<NUMPIXELS;i++){
    if(i < altura){
      leds.G[i]=255;
      leds.B[i]=0;
    }else{
      leds.G[i]= 0;
      leds.B[i]=50;
    }
  }
}


void loop() {
  //put your main code here, to run repeatedly:
  izquierdo.distancia = distancia_cm(izquierdo.disparo, izquierdo.eco);
  derecho.distancia = distancia_cm(derecho.disparo, derecho.eco);
  parlante.izquierdo_altura = tono_grave(izquierdo.distancia);
  parlante.derecho_altura = tono_agudo(derecho.distancia);
  tono_izquierdo.play(parlante.izquierdo_altura);
  tono_derecho.play(parlante.derecho_altura);
  if(count == 128){
    leds_altura_izquierdo(parlante.izquierdo_altura);
    leds_altura_derecho(parlante.derecho_altura);
    print_leds();
    count = 0;
  }
  count++;
}
