#include <Adafruit_NeoPixel.h>
#define PIN_RING 5
#define NUMPIXELS 16
#define BAUDRATE 9600 //9600 o 115200
#define INITIAL 0
#define PIN_TAPETE 2
#define NUM_PERILLAS 2
#define PERRILLA_1 A1
#define PERRILLA_2 A2
#define PASOS 4
#define ESCALA 1023
#define VALOR_PASO (ESCALA/PASOS)
#define PIN_CORRECTO 4
#define PIN_INCORRECTO 3
#define PIN_SETUP 6
#define VELOCIDAD 0 //no tocar
#define SECUENCIA 1 //no tocar
#define ON 0x90 //nota midi ON
#define OFF 0x80 //nota midi OFF

struct Leds
{
    unsigned int R[NUMPIXELS];
    unsigned int G[NUMPIXELS];
    unsigned int B[NUMPIXELS];
};

Leds leds;

struct Perillas
{
    unsigned const int pin_number[NUM_PERILLAS];
    unsigned int valor[NUM_PERILLAS];
    unsigned const int pasos;
    unsigned const int escala;
};

struct Perillas perillas
{
    {PERRILLA_1, PERRILLA_2},
    {INITIAL, INITIAL},
    PASOS,
    ESCALA,
};

unsigned const int velocidad[PASOS] = { 1500, 1000, 750, 500};

struct Secuencia
{
    unsigned const int selector[PASOS][NUMPIXELS];
    unsigned int posicion;
    unsigned int anterior;
};

struct Secuencia secuencia
{
    {
        {1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0},
        {1,0,2,0,1,0,2,0,1,0,2,0,1,0,2,0},
        {0,3,1,0,0,3,1,0,0,3,1,0,0,3,1,0},
        {3,2,1,0,3,2,1,0,3,2,1,0,3,2,1,0},
    },
    INITIAL,
    PASOS
};

unsigned int configuration[NUM_PERILLAS] =
{
    INITIAL, //VELOCIDAD
    INITIAL  //SECUENCIA
};

unsigned const notas[PASOS] = {60, 62, 64, 66};

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN_RING, NEO_GRB + NEO_KHZ800);

bool value_tape = false;

//prototipes
void print_leds(void);
void analogs_reads(void);
void config_print_leds(void);
void perillas_reads(void);
void jump(void);
void send_midi_sound(void);

void setup(){
  pixels.begin();
  pinMode(PIN_TAPETE, INPUT);
  pinMode(PIN_CORRECTO, OUTPUT);
  pinMode(PIN_INCORRECTO, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(PIN_TAPETE), jump, FALLING);
  Serial.begin(BAUDRATE);
}

void loop(){
    if(digitalRead(PIN_SETUP)){
        while(digitalRead(PIN_SETUP)){
            perillas_reads();
            config_print_leds();
            print_leds();
            delay(velocidad[configuration[VELOCIDAD]]);
            secuencia.posicion++;
            if(secuencia.posicion == NUMPIXELS)
                secuencia.posicion = 0;
                secuencia.anterior = NUMPIXELS;
        }
        secuencia.posicion = 0;
    }
    config_print_leds();
    print_leds();
    send_midi_sound();
    value_tape = false;
    delay(velocidad[configuration[VELOCIDAD]]/2);
    Serial.println(secuencia.selector[configuration[SECUENCIA]][secuencia.posicion]);
    if(value_tape && secuencia.selector[configuration[SECUENCIA]][secuencia.posicion]!=0){
        digitalWrite(PIN_CORRECTO, HIGH);
        digitalWrite(PIN_INCORRECTO, LOW);
        value_tape = false;
    }else if(value_tape == 0 && secuencia.selector[configuration[SECUENCIA]][secuencia.posicion]==0){
        digitalWrite(PIN_CORRECTO, HIGH);
        digitalWrite(PIN_INCORRECTO, LOW);
        value_tape = false;
    }else{
        digitalWrite(PIN_CORRECTO, LOW);
        digitalWrite(PIN_INCORRECTO, HIGH);
    }
    delay(velocidad[configuration[VELOCIDAD]]/2);
    digitalWrite(PIN_CORRECTO, LOW);
    digitalWrite(PIN_INCORRECTO, LOW);
    secuencia.anterior = secuencia.posicion;
    secuencia.posicion++;
    if(secuencia.posicion == NUMPIXELS)
        secuencia.posicion = 0;
}

void jump(){
    value_tape = true;
}

void send_midi_sound(void){
    Serial.write(OFF);
    Serial.write(notas[secuencia.selector[configuration[SECUENCIA]][secuencia.anterior]]);
    Serial.write(90);
    Serial.write(ON);
    Serial.write(notas[secuencia.selector[configuration[SECUENCIA]][secuencia.posicion]]);
    Serial.write(90);
}

void config_print_leds(void){
    for(int i=0;i<NUMPIXELS;i++){
        switch(secuencia.selector[configuration[SECUENCIA]][i])
        {
            case 0:
                leds.R[i]=0;
                leds.G[i]=0;
                leds.B[i]=0;
                break;
            case 1:
                leds.R[i]=0;
                leds.G[i]=255;
                leds.B[i]=0;
                break;
            case 2:
                leds.R[i]=255;
                leds.G[i]=0;
                leds.B[i]=0;
                break;
            case 3:
                leds.R[i]=0;
                leds.G[i]=0;
                leds.B[i]=50;
                break;
        }
    }
    leds.R[secuencia.posicion]=255;
    leds.G[secuencia.posicion]=255;
    leds.B[secuencia.posicion]=255;
}

void print_leds(){
  for(int i=0;i<NUMPIXELS;i++){
    //pixels.setPixelColor(i, pixels.Color(0,255,0));
    pixels.setPixelColor(i, pixels.Color(leds.R[i],leds.G[i],leds.B[i]));
    pixels.show();
  }
}

//Lectura de perillas análogicas entregando los pasos
void perillas_reads(void){
   // Lectura de sensores analógicos
    for (int i = 0; i < NUM_PERILLAS; i++)
    {
       configuration[i] = analogRead(perillas.pin_number[i])/VALOR_PASO;
    }
}

