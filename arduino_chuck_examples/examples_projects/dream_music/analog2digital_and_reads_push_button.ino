/*
 * @file digital_reads.ino
 * @brief Permite hacer diferentes lecturas digitales optimizandolas y
 *  enviándolas por puerto serial
 * @author: Johnny Cubides
 * contact: catalejoeducacion@gmail.com, jgcubidesc@gmail.com
 * date: Oct 02, 2018
 * license: GPL
 */
// Máxima cantidad de sensores analógicos a conectar
#define MAX_ANALOG_SENSORS 6 //Poner la cantidad de sensores que usted vaya a usar
#define BAUDRATE 115200 //9600 o 115200
#define DELAY 1 //Tiempo en milisegundos
#define INITIAL 0

//
struct Analog2Digital_Sensor{
    unsigned int pin_number[MAX_ANALOG_SENSORS]; //A0, A1, A2...
    unsigned int value_analog[MAX_ANALOG_SENSORS];
    bool logic[MAX_ANALOG_SENSORS]; //Lógica del sensor 0->Positiva, 1->Negativa
    unsigned int threshold[MAX_ANALOG_SENSORS];
    unsigned int digital_value; //All array
    unsigned int digital_value_old; // All array
};

struct Analog2Digital_Sensor analog2digital_sensor = {
    {A0,      A1,      A2,      A3,      A4,      A5},
    {INITIAL, INITIAL, INITIAL, INITIAL, INITIAL, INITIAL},
    {1,       1,       1,       1,       1,       1}, // 0->lógica positiva, 1-> lógica negativa
    {1000,    1000,    1000,    1000,    1000,    1000}, // umbral[0, 1023]
    INITIAL,
    INITIAL
};

bool *logic;
unsigned int *threshold, *digital_value, *digital_value_old, *value_analog;

// Convierte analogo a digital y envía dato
// el switch, se refiere a que debe ser pulsado para
// intercambiar estado, si no se hace, permanece en el último estado
void send_analog_2_digital_push_button(void);

char bytes[10];

void setup()
{
    // Velocidad de comunicación con el PC
    Serial.begin(BAUDRATE);
    while(true){
        int nRead = Serial.readBytesUntil('\n', bytes, 10);
        if(nRead > 0)
            Serial.println("ok");
    }
    Serial.println("ok");
}

// Ejecución de programa principal
void loop()
{
    send_analog_2_digital_push_button();
}

void send_analog_2_digital_push_button(void)
{
    for (int i = 0; i < MAX_ANALOG_SENSORS; i++)
    {
        analog2digital_sensor.value_analog[i] = analogRead(analog2digital_sensor.pin_number[i]);
    }
    for (int i = 0; i < MAX_ANALOG_SENSORS; i++)
    {
        logic = &analog2digital_sensor.logic[i];
        threshold = &analog2digital_sensor.threshold[i];
        value_analog = &analog2digital_sensor.value_analog[i];
        analog2digital_sensor.digital_value |= ((int)(*logic^((*value_analog > *threshold)?1:0)))<<i;
    }
    // Envío de lectura digital
    if(analog2digital_sensor.digital_value != analog2digital_sensor.digital_value_old){
        // Si tiene solo éstas entradas digitales, puede omitir la siguiente línea comentandola
        Serial.println("P");
        // Enviando el valor de los sensores digitales
        Serial.println(analog2digital_sensor.digital_value);
        analog2digital_sensor.digital_value_old = analog2digital_sensor.digital_value;
    }
    // Fin de transmisión de un paquete
    analog2digital_sensor.digital_value = 0;
    delay(DELAY);
}
