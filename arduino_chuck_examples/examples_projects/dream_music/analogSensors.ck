/*
 * @file analogSensors.ck
 * @brief Permite capturar dese el puerto serial diferentes lecturas
 *  de sensores analógicos e interpretar desde ChucK.
 * @author: Johnny Cubides
 * contact: catalejoeducacion@gmail.com, jgcubidesc@gmail.com
 * date: Oct 03, 2018
 * license: GPL
 */
//*********** CONFIGURACION PUERTO SERIAL *************
<<<"Listando puertos seriales">>>;
SerialIO.list() @=> string list[];
for(int i; i < list.size(); i++){
   <<< i, ":", list[i] >>>;
}
<<<"Iniciando puerto serial">>>;
SerialIO arduinoCom;
//Seleccionar el puerto serial de interés
//con el índice de la lista anterior
arduinoCom.open(0, SerialIO.B115200, SerialIO.ASCII);
4::second => now;
<<<"open serial">>>;
"lineStart" => string line; // Guarda los valores recogidos por el puerto serial

arduinoCom <= "?\r\n";
2::second => now;
arduinoCom <= "?\r\n";
2::second => now;
arduinoCom <= "?\r\n";
2::second => now;
<<<"<= ?\n">>>;
//arduinoCom <= '?';
//arduinoCom <= '?';

//chout <= "here" <= line <= IO.nl();
//50::ms => now;
arduinoCom.onLine() => now;
arduinoCom.getLine() => line;
//chout <= "here" <= line <= IO.nl();
<<<line, "firts serial">>>;
while(line != "ok")
{
    <<<line, "while1">>>;
    arduinoCom <= "?";
    arduinoCom.onLine() => now;
    arduinoCom.getLine() => line;
    50::ms => now;
}

6 => int MAX_PUSH_SENSOR; //Cantidad de sensores

StkInstrument inst[MAX_PUSH_SENSOR];

Sitar sitar1            @=> inst[0];    //1° instrumento
Mandolin mandolin1      @=> inst[1];    //2° instrumento
Clarinet clarinet1      @=> inst[2];    //3° instrumento
BlowBotl blowbolt1      @=> inst[3];    //4° instrumento
Flute flute1            @=> inst[4];    //5° instrumento
Saxofony saxofony1      @=> inst[5];    //6° instrumento


/*Nota midi por instrimento:
  1   2   3   4   5   6*/
[60, 61, 62, 62, 63, 64] @=> int midi_a[];
/* Volumen para cada instrumento:
   1,  2,  3,  4,  5,  6*/
[0.5,0.5,0.5,0.5,0.5,0.5] @=> float volumen_a[];

//Asignación de configuraciones por cada instrumento
for(0 => int i; i< MAX_PUSH_SENSOR; i++)
{
    inst[i] => dac;
    midi_a[i] => Std.mtof => inst[i].freq;
}

// Variables
int valuePush;
50::ms => dur myTime; // Cada cuanto se actualizarán los sonidos

fun void readSerial()
{
    while(true)
    {
        arduinoCom.onLine() => now;
        arduinoCom.getLine() => line;
        if(line == "P")
        {
            arduinoCom.onLine() => now;
            arduinoCom.getLine() => line;
            while(line == ""){
                arduinoCom.onLine() => now;
                arduinoCom.getLine() => line;
            }
            Std.atoi(line) => valuePush;
        }
    }
}

fun void playSond()
{
    while(true)
    {
        for(0 => int i; i< MAX_PUSH_SENSOR; i++)
        {
            if(valuePush & (1 << i))
                1 => inst[i].noteOn;
            else
                1 => inst[i].noteOff;
        }
        myTime => now;
    }
}

//spork ~ readSerial();
//spork ~ playSond();

while(true) 1::second => now;

