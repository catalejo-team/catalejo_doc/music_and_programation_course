BeeThree instrumento[3];
instrumento[0] => dac;
instrumento[1] => dac;
instrumento[2] => dac;

[
    [32, 42, 49],
    [30, 40, 35],
    [28, 38, 25]
] @=> int notas[][];

[
    [1.0, 0.5, 0.8],
    [1.0, 0.5, 0.8],
    [1.0, 0.5, 0.8]
] @=> float vel[][];

[1.0, 1.0, 1.0] @=> float tiempo[];

3 => int limit;

while(true){
    for( 0 => int i; i < limit; i++ ){

        // asignando la nota que sonará
        notas[0][i] => Std.mtof => instrumento[0].freq;
        notas[1][i] => Std.mtof => instrumento[1].freq;
        notas[2][i] => Std.mtof => instrumento[2].freq;

        // Velocidad ... fuerza.. volumen
        vel[0][i] => instrumento[0].noteOn;
        vel[1][i] => instrumento[1].noteOn;
        vel[2][i] => instrumento[2].noteOn;

        tiempo[i]:: second => now;
    }
}

//30 => Std.mtof => instrumento.freq;
//1 => instrumento.noteOn;
//10:: second => now;
//1 => instrumento.noteOff;
//4:: second => now;
