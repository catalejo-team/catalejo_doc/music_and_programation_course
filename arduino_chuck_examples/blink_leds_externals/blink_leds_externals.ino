/*
* Brief: Encendido de un LED conectado de manera externa a la
* placa.
* Author: Johnny Cubides
* License: GPL
*/
//LED1 está puesto en el pin 12
#define LED1 12

void setup(){
    //Poner el pin donde está el LED1 como salida de información
    pinMode(LED1, OUTPUT);
    //Activar la comunicación serial con el computador
    Serial.begin(9600);
}

void loop(){
    //Poner el LED en 1 (encendido en lógica positiva)
    digitalWrite(LED1, HIGH);
    //Reportar al computador el estado del LED, LED1 encendido
    Serial.println("LED1 encendido");
    //Espera un segundo
    delay(1000);
    //Poner el LED en 0 (encendido en lógica positiva)
    digitalWrite(LED1, LOW);
    //Reportar al computador el estado del LED, LED1 apagado
    Serial.println("LED1 apagado");
    //Espera un segundo
    delay(1000);
}
