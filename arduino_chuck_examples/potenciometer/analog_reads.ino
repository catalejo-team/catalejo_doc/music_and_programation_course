/*
 * @file analog_reads.ino
 * @brief Permite hacer diferentes lecturas análogas
 *  enviándolas por puerto serial
 * @author: Johnny Cubides
 * contact: catalejoeducacion@gmail.com, jgcubidesc@gmail.com
 * date: Oct 02, 2018
 * license: GPL
 */
// Máxima cantidad de sensores analógicos a conectar
#define MAX_SENSORS_ANALOG 1 //Poner la cantidad de sensores que usted vaya a usar
#define BAUDRATE 9600 //9600 o 115200
#define DELAY 10 //Tiempo en milisegundos
#define INITIAL 0

// Definición del sensor: nombre, pin, valor análogo
struct Analog_Sensor{
    String sensor_name;
    int pin_number;     //A0, A1, A2...
    int sensitivity;    //sensible [0,1,2,3,4,5,6...]insensible
    int analog_value;
    int analog_value_old;
};

//Punteros
String *sensor_name;
int *analog_sensitivity, *analog_value, *analog_value_old;

// Total de sensores máximo 6 para arduino UNO del A0 al A5
struct Analog_Sensor analog_sensors[MAX_SENSORS_ANALOG] = {
    // La cantidad de sensores a declarar debe coincidir con
    // el número indicado en MAX_SENSORS_ANALOG
    {"S1", A0, 6, INITIAL, INITIAL},
};

// Enviar lecturas analogas
void send_analog_readings(void);

void setup()
{
    // Velocidad de comuicación con el PC
    Serial.begin(BAUDRATE);
}

// Ejecución de programa principal
void loop()
{
    send_analog_readings();
}

void send_analog_readings(void)
{
    // Lectura de sensores analógicos
    for (int i = 0; i < MAX_SENSORS_ANALOG; i++)
    {
       analog_sensors[i].analog_value = analogRead(analog_sensors[i].pin_number);
    }
    // Envío de lecturas analógcas
    for (int i = 0; i < MAX_SENSORS_ANALOG; i++)
    {
        //única búsqueda y direccionamiento a punteros
        sensor_name = &analog_sensors[i].sensor_name;
        analog_value = &analog_sensors[i].analog_value;
        analog_value_old = &analog_sensors[i].analog_value_old;
        analog_sensitivity = &analog_sensors[i].sensitivity;
        //Para manejo de sensibilidad descomente éste if y comente el siguiente if
        if((*analog_value < *analog_value_old-*analog_sensitivity)||(*analog_value > *analog_value_old+*analog_sensitivity))
        //Sin manejo de sensibilidad descomente éste if y comente el anterior if
        //if(*analog_value != *analog_value_old)
        {
            Serial.println(*sensor_name);
            Serial.println(*analog_value);
            *analog_value_old = *analog_value;
        }
    }
    // Fin de transmisión de un paquete
    delay(DELAY);
}
