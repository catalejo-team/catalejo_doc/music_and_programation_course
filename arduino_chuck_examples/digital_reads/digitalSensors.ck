/*
 * @file digitalSensors.ck
 * @brief Permite capturar dese el puerto serial diferentes lecturas
 *  de sensores digitales e interpretar desde ChucK.
 * @author: Johnny Cubides
 * contact: catalejoeducacion@gmail.com, jgcubidesc@gmail.com
 * date: Oct 03, 2018
 * license: GPL
 */
//*********** CONFIGURACION PUERTO SERIAL *************
<<<"Listando puertos seriales">>>;
SerialIO.list() @=> string list[];
for(int i; i < list.size(); i++){
   <<< i, ":", list[i] >>>;
}
<<<"Iniciando puerto serial">>>;
SerialIO arduinoCom;
//Seleccionar el puerto serial de interés
//con el índice de la lista anterior
arduinoCom.open(0, SerialIO.B9600, SerialIO.ASCII);

3 => int MAX_DIGITAL_SENSOR;
SinOsc osc[MAX_DIGITAL_SENSOR];
//Los siguientes 2 arregos corresponden a la posición decada sensor,
//en éste ejemplo son tres sensores que tienen asociadas 3 notas con
//su respectivo volumen.
[60 ,  61,  62] @=> int midi[];
[0.5, 0.5, 0.5] @=> float volumen[];

for(0 => int i; i< MAX_DIGITAL_SENSOR; i++)
{
    osc[i] => dac;
    midi[i] => Std.mtof => osc[i].freq;
    0 => osc[i].gain;
}

// Variables
string line; // Guarda los valores recogidos por el puerto serial
int valueSensors; // Guarda el valor de los sensores
50::ms => dur myTime; // Cada cuanto se actualizarán los sonidos

fun void readSerial()
{
    while(true)
    {
        arduinoCom.onLine() => now;
        arduinoCom.getLine() => line;
        if(line == "D")
        {
            arduinoCom.onLine() => now;
            arduinoCom.getLine() => line;
            Std.atoi(line) => valueSensors;
        }
    }
}

fun void playSond()
{
    while(true)
    {
        for(0 => int i; i< MAX_DIGITAL_SENSOR; i++)
        {
            volumen[i]*(valueSensors & (1 << i)) => osc[i].gain;
        }
        myTime => now;
    }
}

spork ~ readSerial();
spork ~ playSond();

while(true) 1::second => now;

