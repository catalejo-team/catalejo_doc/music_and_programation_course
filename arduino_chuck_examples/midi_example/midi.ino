#define ON 0x90
#define OFF 0x80

void setup()
{
    Serial.begin(115200);
}

void loop()
{
   Serial.write(ON);
   Serial.write(60);
   Serial.write(90);
   delay(500);
   Serial.write(OFF);
   Serial.write(60);
   Serial.write(90);
   delay(500);
   Serial.write(ON);
   Serial.write(65);
   Serial.write(90);
   delay(500);
   Serial.write(OFF);
   Serial.write(65);
   Serial.write(90);
   delay(500);
}

