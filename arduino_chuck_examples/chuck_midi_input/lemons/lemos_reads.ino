/*
 * @file digital_reads.ino
 * @brief Permite hacer diferentes lecturas digitales optimizandolas y
 *  enviándolas por puerto serial
 * @author: Johnny Cubides
 * contact: catalejoeducacion@gmail.com, jgcubidesc@gmail.com
 * date: Oct 02, 2018
 * license: GPL
 *
 * Peso dado a cada sensor
 * 128
 * |64
 * ||32
 * |||16
 * ||||8421
 * ||||||||
 * 76543210 --> Posición de cada sensor
 * ||||||||_ primer sensor
 * |||||||__ segundo sensor
 * ||||||___ tercer sensor
 * |||||____ Cuarto sensor
 * ||||_____ Quinto sensor
 * |||______ Sexto sensor
 * ||_______ Séptimo sensor
 * |________ Octavo sensor
 *
 * Por cada sensor actico, (un uno lógico), al digital_value se suma un peso.
 * Ejemplo:
 *  pimer sensor activo         --> digital_value = 1
 *  segundo sensor activo       --> digital_value = 2
 *  tercer sensor activo        --> digital_value = 4
 *  los tres sensores activos   --> digital_value = 4+2+1 = 7
 *  Ocho sensores activos       --> digital_value = 128+64+32+16+8+4+2+1 = 255
 * Nota:
 *  No hay ninguna combinación que permita obtener el mismo resultado, es así
 *  que se puede distinguir qué sensor o qué sensores están activos.
 */

// Máxima cantidad de sensores analógicos a conectar
#define MAX_ANALOG_SENSORS 6 //Poner la cantidad de sensores que usted vaya a usar
#define BAUDRATE 115200 //9600 o 115200
#define DELAY 1 //Tiempo en milisegundos
#define INITIAL 0

#define ON 0x90
#define OFF 0x80

#define SEN 950

//
struct Analog2Digital_Sensor{
    unsigned int pin_number[MAX_ANALOG_SENSORS]; //A0, A1, A2...
    unsigned int value_analog[MAX_ANALOG_SENSORS];
    bool logic[MAX_ANALOG_SENSORS]; //Lógica del sensor 0->Positiva, 1->Negativa
    unsigned int threshold[MAX_ANALOG_SENSORS];
    bool digital_value[MAX_ANALOG_SENSORS]; //All array
    bool digital_value_old[MAX_ANALOG_SENSORS]; // All array
    unsigned int note[MAX_ANALOG_SENSORS]; // All array
};

struct Analog2Digital_Sensor analog2digital_sensor = {
    {A0, A1, A2, A3, A4, A5},
    {INITIAL, INITIAL, INITIAL, INITIAL, INITIAL, INITIAL},
    {1, 1, 1, 1, 1, 1}, //0 -> lógica positiva, 1 -> lógica negativa
    {800, SEN, SEN, SEN, SEN, SEN}, // umbral[0, 1023]
    {INITIAL, INITIAL, INITIAL, INITIAL, INITIAL, INITIAL},
    {INITIAL, INITIAL, INITIAL, INITIAL, INITIAL, INITIAL},
    {60, 61, 62, 63, 64, 65}
};

bool *logic, *digital_value, *digital_value_old;
unsigned int *threshold, *value_analog, *note;

// Convierte analogo a digital y envía dato
// el switch, se refiere a que debe ser pulsado para
// intercambiar estado, si no se hace, permanece en el último estado
void send_analog_2_digital_retractable_switch(void);


void setup()
{
    // Velocidad de comunicación con el PC
    Serial.begin(BAUDRATE);
}

// Ejecución de programa principal
void loop()
{
    send_analog_2_digital_retractable_switch();
}

void send_analog_2_digital_retractable_switch(void)
{
    for (int i = 0; i < MAX_ANALOG_SENSORS; i++)
    {
        analog2digital_sensor.value_analog[i] = analogRead(analog2digital_sensor.pin_number[i]);
    }
    for (int i = 0; i < MAX_ANALOG_SENSORS; i++)
    {
        // apuntando al recorrer cada sensor
        logic = &analog2digital_sensor.logic[i];
        threshold = &analog2digital_sensor.threshold[i];
        value_analog = &analog2digital_sensor.value_analog[i];
        digital_value = &analog2digital_sensor.digital_value[i];
        digital_value_old = &analog2digital_sensor.digital_value_old[i];
        note = &analog2digital_sensor.note[i];
        // leyendo estado
        *digital_value = *logic^((*value_analog > *threshold)?1:0);
        // si cambia el estado debe ser reportado por puerto midi
        if(*digital_value != *digital_value_old){
          /* Serial.println(i); */
          /* Serial.println(*value_analog); */
          Serial.write((*digital_value)?ON:OFF); //encender o apagar nota
          Serial.write(*note); //enviar nota
          Serial.write(90); // por ahora la fuerza no es considerada en el destinatario
        }
        // guardando la anterior lectura para usarla en una nueva lectura de ciclo
        *digital_value_old = * digital_value;
    }
}
