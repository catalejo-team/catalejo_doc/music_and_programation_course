MidiIn min;
1 => int port;

if( !min.open(port) )
{
  <<< "Error: MIDI port did not open on port: ", port >>>;
  me.exit();
}

MidiMsg msg;

Rhodey piano => dac;

while( true )
{
  min => now; // advance when receive MIDI msg
  while( min.recv(msg) )
  {
    // <<< msg.data1, msg.data2, msg.data3 >>>;
    if (msg.data1 == 144) {
      Std.mtof(msg.data2) => piano.freq;
      //msg.data3/127.0 => piano.gain;
      msg.data3/500.0 => piano.gain;
      1 => piano.noteOn;
    }
    else {
      1 => piano.noteOff;
    }
  }
}
